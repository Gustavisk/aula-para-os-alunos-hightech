import java.util.ArrayList;
import java.util.List;

public class GerenciadorRegistroCliente extends GerenciadorRegistroGenerico<Cliente> {

    @Override
    public void salvar(Cliente obj) {
        super.salvar(obj);
    }

    @Override
    public void excluir(Cliente obj) {
        super.excluir(obj);
    }

    @Override
    public Cliente pesquisar(Cliente obj) {
        return super.pesquisar(obj);
    }

    @Override
    public void imprimirTodos() {
        super.imprimirTodos();
    }

    public List<Cliente> getListaHomens() {
        List<Cliente> listaDeHomens = new ArrayList<>();

        for (Cliente cliente: registros) {
            if(cliente.getSexo() == 'M' || cliente.getSexo() == 'm') {
                listaDeHomens.add(cliente);
            }
        }

        return listaDeHomens;
    }

    public List<Cliente> getListaMulheres() {
        List<Cliente> listaDeMulheres = new ArrayList<>();

        for (Cliente cliente: registros) {
            if(cliente.getSexo() == 'F' || cliente.getSexo() == 'f') {
                listaDeMulheres.add(cliente);
            }
        }

        return listaDeMulheres;
    }

    public List<Cliente> getLista() {
        return registros;
    }

}
