import java.util.ArrayList;
import java.util.List;

public class GerenciadorRegistroGenerico<T> implements GerenciadorRegistro<T> {

    protected List<T> registros = new ArrayList<>();

    @Override
    public void salvar(T obj) {
        registros.add(obj);
    }

    @Override
    public void excluir(T obj) {
        registros.remove(obj);
    }

    @Override
    public T pesquisar(T obj) {
        for (T objeto: registros){
            if(obj.equals(objeto)) {
                return objeto;
            }
        }
        return null;
    }

    @Override
    public void imprimirTodos() {
        for (T objeto: registros) {
            System.out.println(objeto.toString());
        }
    }
}
